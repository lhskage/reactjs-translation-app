export const setStorage = (key, value) => {
    localStorage.setItem(key, value)
}   

export const getStorage = (key) => {
    const storedValue = localStorage.getItem(key)
    if(!storedValue) return false
    return storedValue
}