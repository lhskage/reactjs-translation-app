import React from 'react'
import { getStorage } from '../../utils/localStorage'
import { useHistory } from 'react-router-dom'
import '../../css/Profile.css'

const Profile = () => {

    const history = useHistory()
    const name = getStorage('user')
    const translations = getStorage('translations')

    const showTranslations = (translations) => {
        if (translations.length > 0) {
            translations = translations.split(',')
            return (
                translations.map((translation) => {
                    return (
                        <div id="translations">
                            
                            <ul id="translationList">
                                <li>  
                                    {translation}                                                              
                               </li>
                            </ul>
                        </div>
                    )
                })
            )
        } else {
            return (
                <section>
                    <p>You dont have any translations yet</p>
                </section>
            )
        }
    }

    const onLogoutClicked = () => {
        localStorage.clear()
        history.push('/');
    }

    const onGoBackToTranslationClicked = () => {
        history.push('/translation')
    }

    return (
        <div id="profilePage">
            <div id="titleAndLogout">
                <h1>Welcome to your profile, {name}</h1>
                <button id="logoutButton" type="submit" onClick={onLogoutClicked}>Logout</button>
            </div>
            <section>
                <h2>Your last 10 translations</h2>
                {showTranslations(translations)}
                <button id="backToTranslateButton" onClick={onGoBackToTranslationClicked}>Go to translation</button>
            </section>
        </div>
    )
}

export default Profile