import React from 'react'
import LoginForm from '../forms/LoginForm'
import { setStorage } from '../../utils/localStorage'
import '../../css/Login.css'

const Login = () => {

    const handleGoClicked = ({ username }) => {
        setStorage('user', username)
    }

    return (
        <div id="loginContainer">
            <header>
                <hgroup>
                    <h2>A.W.E.S.O.M.-O 4000</h2>
                </hgroup>
            </header>
            <section>
                <LoginForm onLoginComplete={handleGoClicked}></LoginForm>
            </section>
        </div>
    )
}

export default Login