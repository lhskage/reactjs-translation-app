import React, {useState} from 'react'
import TranslationForm from '../forms/TranslationForm'
import { setStorage, getStorage } from '../../utils/localStorage'
import TranslationOutput from '../TranslationOutput'
import '../../css/Translation.css'

const Translation = () => {

    const [translateText, setTranslateText] = useState([])

    const saveTranslationToLocalStorage = textToTranslate =>  {
        setTranslateText(textToTranslate)
        let translatedTexts = getStorage('translations')
        translatedTexts = translatedTexts ? translatedTexts.split(',') : []
        if(translatedTexts.length >= 10) {
            translatedTexts.shift()
            translatedTexts.push(textToTranslate.join(''))
        } else {
            translatedTexts.push(textToTranslate.join(''))
        }
        setStorage('translations', translatedTexts)
        console.log('(PARENT - Translation) Translations: ', textToTranslate)
    }

    const handleTranslateClicked = ({textToTranslate}) => {
        saveTranslationToLocalStorage(textToTranslate)
    }

    return (
        <div id="translationContainer">
            <TranslationForm onTranslateStarted={handleTranslateClicked}></TranslationForm>
            <TranslationOutput onTranslateClicked={translateText}/>
        </div>
    )
}

export default Translation

