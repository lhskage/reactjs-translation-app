import React from 'react'
import '../css/TranslationOutput.css'

const TranslationOutput = (props) => {

    const translateToImage = (textToTranslate) => {
        return (
            textToTranslate
            .map((letter) => (letter + '.png'))
            .map((image) => {
                return (
                    <img src={image} alt='random' width='50'/>
                )
            })
        )
    }

    return (
        <div>
            <div id="translationOutput">
                {translateToImage(props.onTranslateClicked)}
            </div>
        </div>
    )
}

export default TranslationOutput