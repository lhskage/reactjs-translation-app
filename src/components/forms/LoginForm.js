import React, { useState } from 'react'
import { useHistory } from 'react-router-dom'
import '../../css/LoginForm.css'

const LoginForm = props => {

    const history = useHistory();

    const [username, setUsername] = useState('')
    const [awesomo, setAwesomo] = useState('Awesom-O.png')

    const onLoginClicked = event => {
        props.onLoginComplete({
            username
        })

        if (!isUsernameEmpty(username)) {
            history.push('/translation')
        } else {
            setAwesomo('Awesom-NO.png')
        }
    }

    const isUsernameEmpty = (username) => {
        if (username === '') {
            return true
        } else {
            return false
        }
    }


    const onUsernameChanged = event => {
        setUsername(event.target.value.trim())
    }

    return (
        <div>
            <section id="awesom-o">
                <img src={awesomo} alt="a robot lost in translation"/>
            </section>
            <form id="loginForm">
                <input id="nameInput" type="text" placeholder=" What's your name?" onChange={onUsernameChanged} />
                <button id="loginButton" type="submit" onClick={onLoginClicked}>Go</button>
            </form>
        </div>

    )
}

export default LoginForm