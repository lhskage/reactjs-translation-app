import React, { useState } from 'react'
import {useHistory} from 'react-router-dom'
import '../../css/TranslationForm.css'

const TranslationForm = props => {

    const [textToTranslate, setTextToTranslate] = useState([])
    const history = useHistory()

    const onTranslateClicked = event => {
        if(textToTranslate === undefined || textToTranslate.length === 0) {
            window.alert('Awesom-O can\'t compute')
        } else {
            props.onTranslateStarted({
            textToTranslate
        })
        }
        
    }

    const onProfileClicked = (event) => {
        history.push('/myProfile')
    }

    const onTranslationInputChanged = event => {
        setTextToTranslate(event.target.value.trim().toLowerCase().split(''))
    }
    

    return (
        <div id="translationPage">
            <section id="profile">                    
                <button id="profileButton" type="submit" onClick={onProfileClicked}>My Profile</button>
            </section>

            <section id="awesom-go">
                <img src="Awesom-GO.png" alt="Tell awesomo your secrets"/>
            </section>
            
            <form id="translationForm">
                <section>
                    <input id="translationInput" autoComplete='off' type="text" placeholder="Tell secret..." onChange={onTranslationInputChanged}></input>
                    <button id="translateButton" type="button" onClick={onTranslateClicked}>Translate secret</button>
                </section>
            </form>
        </div>
    )
}

export default TranslationForm