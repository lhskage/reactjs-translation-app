import React from 'react';
import './App.css';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from 'react-router-dom'

import Login from './components/containers/Login'
import Translation from './components/containers/Translation'
import Profile from './components/containers/Profile'

function App() {
  return (
	<Router>
		<div className="App">	
        	<Switch>
				<Route path='/login' component={Login}/>
				<Route path='/translation' component={Translation} />
				<Route path='/myProfile' component={Profile} />
				<Route path='/'>
					<Redirect to='/login'/>
				</Route>
        	</Switch>
    	</div>
    </Router>
  );
}

export default App;
